#!/bin/bash
set -x
mkdir -p /home/vagrant/.ssh
cd /home/vagrant/.ssh || exit
curl -o authorized_keys https://raw.githubusercontent.com/hashicorp/vagrant/main/keys/vagrant.pub
chmod 0700 /home/vagrant/.ssh
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant:vagrant /home/vagrant/.ssh
