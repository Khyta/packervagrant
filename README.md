# Ziele

- Ubuntu Server image
- `packer build`  
- `vagrant init`
- `vagrant up`

# Voraussetzungen

- Vagrant  
- Packer
