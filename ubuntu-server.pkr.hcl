packer {
# Um eine Vagrant Box zu machen, benötigt man zuerst eine VirtualBox Box.
# Vagrant kommt am Schluss beim post processor Block.
    required_plugins {
        qemu = {
            version = "~> 1"
            source = "github.com/hashicorp/qemu"
        }
        vagrant = {
            version = "~> 1"
            source = "github.com/hashicorp/vagrant"
        }
    }
}

# data {
# Mit dem data Block, kann man Daten von ausserhalb holen, die man dann im 
# Source Block benutzen kann.
# }

source "qemu" "ubuntu" {
# Der Source Block definiert Konfigurationsblöcke, welche von builder benutzt
# werden. Der Source Block erwartet zwei Labels: 1. Builder Type, 2. Unique
# Name. Ein Source Block kann mehrmals verwendet werden.
    boot_wait = "5s"
    boot_command = [
        "e<wait>",
        "<down><down><down><end>",
        " autoinstall ds=nocloud-net\\;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/", # Mit diesem Command greift Packer auf den Web Server zu, bei dem die Dateien für Cloud-Init gespeichert sind.
        "<f10>"
    ]

    iso_url = "http://releases.ubuntu.com/jammy/ubuntu-22.04.3-live-server-amd64.iso"
    iso_checksum = "sha256:a4acfda10b18da50e2ec50ccaf860d7f20b389df8765611142305c0e911d16fd"
    memory = 8192 # Ohne Memory, gibt es einen Kernel Panic
    ssh_username = "vagrant"
    ssh_password = "vagrant"
    communicator = "ssh"
    ssh_timeout = "20m"
    cpus = 6
    accelerator = "kvm" # set to none if no kvm installed
    format = "qcow2"
    disk_size = "30G"
    http_directory = "http"
    shutdown_command = "echo vagrant | sudo -S shutdown -P now"
}

build {
# Builder verwenden die soucres und können hier noch zusätzliche Informationen,
# wie Gerätename hinzufügen. Eine source kann bei mehreren Builds benutzt
# werden, z.B. um verschiedene Maschinen mit anderen Namen zu erstellen.
    sources = ["source.qemu.ubuntu"]
    provisioner "shell" {
    # Man benutzt Provisioners, um die VM nach dem Booten zu installieren und 
    # konfigurieren. Den einzigen Command, welchen man braucht, ist auf cloud
    # -init zu warten. 
        execute_command = "echo 'vagrant' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
        scripts = [
            "scripts/ssh_keys.sh",
        ]
    }
    provisioner "shell" { # Cloud-init soll mit einem neuen Networkinterface starten
        execute_command = "echo 'vagrant' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
        inline = [
            "rm /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg", 
            "rm /etc/netplan/00-installer-config.yaml",
            "apt install rsync",
            "while [ ! -f /var/lib/cloud/instance/boot-finished ]; do echo 'Waiting for Cloud-Init...'; sleep 1; done"
        ]
      }
    post-processor "vagrant" {
        # Ein post-processor ist optional. Sie können Boxen konvertieren,
        # checksums ausrechnen, artifacts hochladen zu AWS etc. What needs to
        # be done: 1. Create vagrant box 2. Remove artifact build 3. ??? 4.
        # Profit
        keep_input_artifact = false
        provider_override = "libvirt"
    }
}

# TODO provisioner (Wie man das VM Image konfigurieren will) und post processor
# ( * Image created, now what meme * )

